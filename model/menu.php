<?php
include_once("E:/xampp/htdocs/quanlybanhang/database.php");
class Menu
{
  public $_id;
  public $_menu_name;
  public $_menu_link;
  public $_id_parrent;
  public $_database=null;
  public function Menu()
  {
      $this->_id=0;
      $this->_menu_name="NOT SET";
      $this->_menu_link="#";
      $this->_id_parrent=0;
      $this->_database = new Database();
  }
  public function getId(){
    return $this->_id;
  }
  public function getMenuName(){
    return $this->_menu_name;
  }
  public function getLinkMenu(){
    return $this->_menu_link;
  }
  public function getIdParent(){
    return $this->_id_parrent;
  }
  public function setId($value){
    $this->_id=$value;
  }
  public function setMenuName($value){
    $this->_menu_name=$value;
  }
  public function setLinkMenu($value){
    $this->_menu_link=$value;
  }
  public function setIdParent($value){
    $this->_id_parrent=$value;
  }
  public function getListMenu(){
    $sql="select * from menu where id_parrent=0";
    $dataMenu=$this->_database->Readdata($sql);
    $countMenu=count($dataMenu);
    $objectMenu= array();
    for($i=0;$i<$countMenu;$i++){
      $menu=$dataMenu[$i];
      $id=$menu["id"];
      $name=$menu["menu_name"];
      $link=$menu["menu_link"];
      $id_parent=$menu["id_parrent"];
      $obj = new Menu();
      $obj->setId($id);
      $obj->setMenuName($name);
      $obj->setLinkMenu($link);
      $obj->setIdParent($id_parent);
      $objectMenu[]=$obj;
    }
    return $objectMenu;
  }
  public function getListMenucon($id_parent){
    $sql="select * from menu where id_parrent=$id_parent";
    $dataMenucon=$this->_database->Readdata($sql);
    $sophantu=count($dataMenucon);
    $objectMenu=array();
    for($i=0;$i< $sophantu;$i++)
    {
      $menucon=$dataMenucon[$i];
      $id_con=$menucon['id'];
      $menu_name_con=$menucon['menu_name'];
      $menu_link_con=$menucon['menu_link'];
      $id_parent_con=$menucon['id_parrent'];
      $obj = new Menu();
      $obj->setId($id_con);
      $obj->setMenuName($menu_name_con);
      $obj->setLinkMenu($menu_link_con);
      $obj->setIdParent($id_parent_con);
      $objectMenu[]=$obj;
    }
    return $objectMenu;
  }
}


?>
