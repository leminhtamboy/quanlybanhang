<?php
include_once("E:/xampp/htdocs/quanlybanhang/database.php");
class Slider
{
  #Int Primari key
  protected $_slider_id;
  #Varchar
  protected $_slider_tittle;
  #Text
  protected $_slider_description;
  #Varchar
  protected $_slider_image;
  #Int
  protected $_slider_order;
  #Database
  protected $_database=null;

  public function getSliderId(){
      return $this->_slider_id;
  }

  public function getSliderTitle(){
      return $this->_slider_tittle;
  }

  public function getSliderDescription(){
      return $this->_slider_description;
  }

  public function getSliderImage(){
      return $this->_slider_image;
  }

  public function getSliderOrder(){
      return $this->_slider_order;
  }

  public function setSliderId($value){
      $this->_slider_id = $value;
  }

  public function setSliderTitle($value){
      $this->_slider_tittle = $value;
  }

  public function setSliderDescription($value){
      $this->_slider_description = $value;
  }

  public function setSliderImage($value){
      $this->_slider_image = $value;
  }

  public function setSliderOrder($value){
      $this->_slider_order = $value;
  }
  /*
    Construtor Slider()
    NoParam
  */
  public function Slider()
  {
      $this->_slider_id = 0;
      $this->_slider_tittle = "";
      $this->_slider_description = "";
      $this->_slider_image = "";
      $this->_slider_order = 0;
      $this->_database = new Database();
  }
  /***
  function GetListSlider()
  @params: no param
  @return: array Object Slider
  ***/
  public function GetListSlider()
  {
      $sql = "select * from slider";
      $arrayData = $this->_database->Readdata($sql);
      $arrayObject = array();
      $countData = count($arrayData);
      for($i=0;$i<$countData;$i++){
          $slider = $arrayData[$i];
          $slider_id = $slider["slider_id"];
          $slider_title = $slider["slider_title"];
          $slider_description = $slider["slider_description"];
          $slider_image = $slider["slider_image"];
          $slider_order = $slider["slider_order"];
          $objSlider = new Slider();
          $objSlider->setSliderId($slider_id);
          $objSlider->setSliderTitle($slider_title);
          $objSlider->setSliderDescription($slider_description);
          $objSlider->setSliderImage($slider_image);
          $objSlider->setSliderOrder($slider_order);
          $arrayObject[] = $objSlider;
      }
      return $arrayObject;
  }


}



 ?>
