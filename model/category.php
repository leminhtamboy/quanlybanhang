<?php
include_once("E:/xampp/htdocs/quanlybanhang/database.php");
class Category
{
  protected $_category_id ;
  protected $_category_name ;
  protected $_category_image ;
  protected $_category_description;
  protected $_category_order ;
  protected $_database = null;
  public function getCategoryId(){
    return $this->_category_id;
  }
  public function getCategoryName(){
    return $this->_category_name;
  }
  public function getCategoryImage(){
    return $this->_category_image;
  }
  public function getCategoryDescription(){
    return $this->_category_description;
  }
  public function getCategoryOrder(){
    return $this->_category_order;
  }
  public function setCategoryId($value){
    $this->_category_id = $value;
  }
  public function setCategoryName($value){
    $this->_category_name = $value;
  }
  public function setCategoryImage($value){
    $this->_category_image = $value;
  }
  public function setCategoryDescription($value){
    $this->_category_description = $value;
  }
  public function setCategoryOrder($value){
    $this->_category_order = $value;
  }
  public function Category(){
    $this->_category_id = 0;
    $this->_category_name = "Not set";
    $this->category_image = "";
    $this->category_description = "";
    $this->category_order = 0;
    $this->_database = new Database;
  }
  public function getListCategory(){
    $sql = "select * from category";
    $arrayData = $this->_database->Readdata($sql);
    $arrayObject = array();
    $count = count($arrayData);
    foreach($arrayData as $category){
      $category_id = $category["category_id"];
      $category_name = $category["category_name"];
      $category_image = $category["category_image"];
      $category_description = $category["category_description"];
      $category_order = $category["category_order"];
      $objCategory = new Category();
      $objCategory->setCategoryId($category_id);
      $objCategory->setCategoryName($category_name);
      $objCategory->setCategoryImage($category_image);
      $objCategory->setCategoryDescription($category_description);
      $objCategory->setCategoryOrder($category_order);
      $arrayObject[] = $objCategory;
    }
    return $arrayObject;
  }
  public function getCategory($category_id){
    $sql ="select * from category where category_id = $category_id";
    $arrayData = $this->_database->ReadData($sql);
    $category_id = $arrayData[0]['category_id'];
    $category_name = $arrayData[0]['category_name'];
    $category_image = $arrayData[0]['category_image'];
    $this->setCategoryId($category_id);
    $this->setCategoryName($category_name);
    $this->setCategoryImage($category_image);
    return $this;
  }
}
?>
