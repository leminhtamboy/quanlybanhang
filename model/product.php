<?php
$root=Helper::getRootWeb()."/quanlybanhang";
include_once($root."/database.php");
class Product
{
  protected $_product_id;
  protected $_product_name;
  protected $_product_image;
  protected $_product_description;
  protected $_product_longdescription;
  protected $_product_price;
  protected $_product_discount;
  protected $_product_category_id;
  protected $_database;
  public function Product(){
    $this->_database = new Database();
  }
  public function getProductId(){
    return $this->_product_id;
  }
  public function getProductName(){
    return $this->_product_name;
  }
  public function getProductImage(){
    return $this->_product_image;
  }
  /*public function getCategoryDescription(){
    return $this->_category_description;
  }
  public function getCategoryOrder(){
    return $this->_category_order;
  }*/
  public function setProductId($value){
    $this->_product_id = $value;
  }
  public function setProductName($value){
    $this->_product_name = $value;
  }
  public function setProductImage($value){
    $this->_product_image = $value;
  }
  /*public function setCategoryDescription($value){
    $this->_category_description = $value;
  }
  public function setCategoryOrder($value){
    $this->_category_order = $value;
  }*/
  public function getProductByCategory($category_id){
      $sql="select * from product where product_category_id=$category_id";
      $arrayData=$this->_database->Readdata($sql);
      $arrayObjectProduct = array();
      foreach($arrayData as $_product){
        $objProduct = new Product();
        $objProduct->setProductId($_product["product_id"]);
        $objProduct->setProductName($_product["product_name"]);
        $objProduct->setProductImage($_product["product_image"]);
        $arrayObjectProduct[] = $objProduct;
      }
      return $arrayObjectProduct;
  }
  public function getProducts(){
    $sql="select * from product";
    $arrayData=$this->_database->Readdata($sql);
    $arrayObjectProduct = array();
    foreach($arrayData as $_product){
      $objProduct = new Product();
      $objProduct->setProductId($_product["product_id"]);
      $objProduct->setProductName($_product["product_name"]);
      $objProduct->setProductImage($_product["product_image"]);
      $arrayObjectProduct[] = $objProduct;
    }
    return $arrayObjectProduct;
  }
}
?>
