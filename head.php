<?php
include_once("Helper.php");
$helper = new Helper();
$baseUrl = $helper->getBaseUrl();
$pathImage = $helper->getImagePath();
 ?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Điện thoại</title>
    <meta name="description" content="Chatelles est la marque française de slippers personnalisables pour femmes – chaussures parisiennes, intemporelles avec une touche rock" />
    <meta name="keywords" content="Slippers, ballerines, plats, mocassin, personnalisable" />
    <meta name="robots" content="INDEX,FOLLOW" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <link rel="alternate" href="https://www.mychatelles.com/de/" hreflang="de" />
    <link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
    <script type="text/javascript" src="<?php echo $baseUrl; ?>js/script.js"></script>
    <script type="text/javascript">
        optionalZipCountries = ["HK", "IE", "MO", "PA"];
    </script>
		<noscript>
			<img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=1521359351421226&ev=PageView&noscript=1"/>
		</noscript>
    <script type="text/javascript">
        etCurrencyManagerJsConfig = {
            "precision": 0,
            "position": 16,
            "display": 2,
            "zerotext": "",
            "excludecheckout": "1",
            "cutzerodecimal": 1,
            "cutzerodecimal_suffix": "",
            "min_decimal_count": "0"
        };
        try {
            extendProductConfigformatPrice();
        } catch (e) {}
    </script>
    <style>
        li#menu-item-category-40 a {
            color: #d84d3c
        }
    </style>
    <script type="text/javascript">
        $jq = jQuery.noConflict();
        $jq(document).ready(function() {
            var _isReload = 1;
            if (_isReload === 1) {
                return false;
            }
        });
    </script>
</head>
