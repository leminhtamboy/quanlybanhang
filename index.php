<!DOCTYPE html>
<!--[if lt IE 7 ]><html
lang="fr" id="top" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html
lang="fr" id="top" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html
lang="fr" id="top" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html
lang="fr" id="top" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="fr" id="top" class="no-js">
<!--<![endif]-->
<?php include_once("head.php"); ?>

<body class=" cms-index-index cms-home">
    <div class="wrapper">
        <div class="page">
            <script>
                var mainURL = 'https://www.mychatelles.com/fr/';
                function loadFristBlock() {
                    var urlExcuteAjax = mainURL + "" + "supercustom/index/renderfirstblock/";
                    jQuery.ajax({
                        url: urlExcuteAjax,
                        dataType: "html",
                        type: "post",
                        success: function(data) {
                            jQuery("#renderfirstblock").html(data);
                        }
                    });
                }
                function loadSecondBlock() {
                    var urlExcuteAjax = mainURL + "" + "supercustom/index/rendersecondblock/";
                    jQuery.ajax({
                        url: urlExcuteAjax,
                        dataType: "html",
                        type: "post",
                        success: function(data) {
                            jQuery("#rendersecondblock").html(data);
                        }
                    });
                }
                function loadBottomBlock() {
                    var urlExcuteAjax = mainURL + "" + "supercustom/index/renderthirdblock/";
                    jQuery.ajax({
                        url: urlExcuteAjax,
                        dataType: "html",
                        type: "post",
                        success: function(data) {
                            jQuery("#renderthirdblock").html(data);
                        }
                    });
                }
                function loadInstagramBlock() {
                    var urlExcuteAjax = mainURL + "" + "supercustom/index/renderinstagramblock/";
                    jQuery.ajax({
                        url: urlExcuteAjax,
                        dataType: "html",
                        type: "post",
                        success: function(data) {
                            jQuery("#instafeed").html(data);
                        }
                    });
                }
                setTimeout(function() {
                    loadFristBlock();
                    loadSecondBlock();
                    loadBottomBlock();
                    loadInstagramBlock();
                }, 3000);
            </script>
            <a href="#top" class="scrollTo scrolltop">
            </a>
            <header id="header" class="page-header">
                <div class="header-container">
                    <a class="homepage-header-logo" href="<?php $helper->getBaseUrl(); ?>">
                        <h1 style="margin-top: 0 !important;">
                            <!--<img src="#" class="chatelles-logo" />-->
                            LOGO
                        </h1>
                    </a>
                    <div class="account-cart-wrapper">
                        <a href="#" class="skip-account">
                            <i class="chatelles-user-header-icon chatelles-icon-login"></i>
                            <span class="label">Đăng Nhập</span>
                        </a>
                        <div class="header-minicart">
                            <script type="text/javascript">
                                $j = jQuery.noConflict();
                                $j(document).ready(function(e) {
                                    $j("#custom-a-minicart").click(function(e) {
                                        console.log($j(window).width());
                                        if ($j(window).width() < 785) {
                                            top.location.href = $j("#header-cart").attr("href");
                                            e.preventDefault();
                                        } else {
                                            if ($j("#header-cart").parent().css("height") == "0px") {
                                                $j("#header-cart").show();
                                            } else {
                                                $j("#header-cart").hide();
                                            }
                                            e.preventDefault();
                                        }
                                    });
                                });
                            </script>
                            <a href="#" id="custom-a-minicart" data-target-element="#header-cart" class="skip-link skip-cart  no-count">
                                <i class="chatelles-user-header-icon chatelles-icon-cart"></i>
                                <span class="label">Giỏ Hàng</span>
                                <span class="count">0</span>
                            </a>
                        </div>
                    </div>
                    <div class="skip-links">
                        <a href="#header-nav" class="skip-link skip-nav" onclick="openParentMenu(this)">
                            <span class="icon-hamburger">
                            <span class="item-1"></span>
                            <span class="item-2"></span>
                            <span class="item-3"></span>
                            </span>
                            <span class="label">Menu</span>
                        </a>
                        <a href="#header-search" class="skip-link skip-search">
                            <span class="icon"></span>
                            <span class="label">Chercher</span>
                        </a>
                    </div>
                    <div class="header-nav-container">
                        <div id="header-nav" class="skip-content">
                            <nav id="navigation" role="navigation">
                                <ul class="nav-primary">
                                    <li id="menu-item-13" class="level0 nav-1 first parent"><a href="#" class="level0 has-children">Sản Phẩm</a></li>
                                    <li id="menu-item-2" class="level0 nav-2 single-column"><a href="#" class="level0 ">Máy Củ</a></li>
                                    <li id="menu-item-1" class="level0 nav-3"><a href="#" class="level0 ">Hỏi Đáp</a></li>
                                    <li id="menu-item-3" class="level0 nav-4 last"><a href="#" class="level0 ">Tin Tức</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div id="header-account" class="skip-content">
                        <div class="links">
                            <ul>
                                <li class="first"><a href="https://www.mychatelles.com/fr/customer/account/" title="Mon compte">Mon compte</a></li>
                                <li><a href="https://www.mychatelles.com/fr/checkout/cart/" title="Mon panier" class="top-link-cart">Mon panier</a></li>
                                <li><a href="https://www.mychatelles.com/fr/checkout/" title="Commander" class="top-link-checkout">Commander</a></li>
                                <li><a href="https://www.mychatelles.com/fr/customer/account/create/" title="S'enregistrer">S'enregistrer</a></li>
                                <li class=" last"><a href="https://www.mychatelles.com/fr/customer/account/login/" title="Connexion">Connexion</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="header-minicart" style="display: block;">
                    <div id="header-cart" class="block block-cart skip-content">
                        <div id="minicart-error-message" class="minicart-message"></div>
                        <div id="minicart-success-message" class="minicart-message"></div>
                        <div class="minicart-wrapper">
                            <p class="block-subtitle">
                                <a class="close skip-link-close" href="#" onclick="jQuery('#header-cart').hide(0);" title="Fermer">&times;</a></p>
                            <p class="empty got">
                                <b>Giỏ Hàng</b> chưa có sản phẩm nào &#9785;</p>
                            <p class="emptylink">
                                <a href="#">Xem sản phẩm</a></p>
                        </div>
                    </div>
                </div>
            </header>
            <style>
                .title-sale {
                    font-weight: bold;
                    cursor: pointer
                }

                .homepage-items-submenu {
                    max-width: 60%;
                    height: auto
                }

                .home-page-submenu-item {
                    width: 14% !important
                }

                .home-page-submenu-item>img {
                    max-width: 60%;
                    height: auto;
                    margin: auto;
                    margin-left: 7%
                }

                .home-page-submenu-item-title {
                    margin-top: -20px
                }

                .homepage-title-submenu {
                    font-size: 11px !important;
                    font-family: "futura-med", got-light !important;
                    font-weight: 500
                }

                .homepage-title-submenu>a {
                    font-family: "futura-med", got-light !important;
                    font-size: 9px !important
                }

                .homepage-subtite-first-title>a {
                    font-size: 11px !important;
                    font-family: "futura-med", got-light !important;
                    font-weight: 500;
                    text-transform: uppercase
                }

                .ipad-category-title {
                    left: 0 !important;
                    text-align: left;
                    margin-left: 14%
                }

                .ipad-category-title-second {
                    left: 0 !important;
                    text-align: left;
                    margin-left: 14%
                }

                .homepage-submenu-ul {
                    margin-left: 15%
                }

                .homepage-submenu-right-sub-li {
                    line-height: 15px !important
                }

                .homepage-submenu-left-ul {
                    margin-left: 15%
                }

                .homepage-submenu-left-ul:first-child {
                    margin-bottom: 32%;
                    margin-top: -7%
                }

                .homepage-submenu-left-sub-li {
                    line-height: 20px !important
                }

                #custom-submenu-sale {
                    border-top: 2px solid #fff
                }

                .homepage-title-submenu>a {
                    line-height: 20px
                }
                .description-second{
                  font-family: sans-serif !important;
                }
                @media only screen and (min-width: 600px) and (max-width: 769px) {
                    .nav-primary {
                        margin-top: 8px
                    }
                    .title-sale {
                        font-weight: bold;
                        cursor: pointer
                    }
                    .homepage-items-submenu {
                        max-width: 60%;
                        height: auto
                    }
                    .home-page-submenu-item {
                        width: 14% !important
                    }
                    .homepage-submenu-left-ul {
                        margin-left: 0
                    }
                    .homepage-submenu-left-ul:first-child {
                        margin-bottom: 21%;
                        margin-top: 0
                    }
                    .homepage-submenu-left-sub-li {
                        line-height: 20px !important
                    }
                    .home-page-submenu-item>img {
                        max-width: 70%;
                        height: auto;
                        margin-bottom: -15%
                    }
                    .home-page-submenu-item-title {
                        margin-top: -20px
                    }
                    .homepage-title-submenu {
                        font-size: 11px !important;
                        font-family: "futura-med", got-light !important;
                        font-weight: 500
                    }
                    .homepage-title-submenu>a {
                        font-family: "futura-med", got-light !important;
                        font-size: 11px !important;
                        font-weight: 500
                    }
                    .homepage-subtite-first-title>a {
                        font-size: 11px !important;
                        font-family: "futura-med", got-light !important;
                        font-weight: 500;
                        text-transform: uppercase;
                        line-height: 30px !important
                    }
                    .ipad-category-title-second {
                        left: 0 !important
                    }
                    .ipad-category-title {
                        left: 0 !important;
                        margin-bottom: 0
                    }
                    .homepage-submenu-right-sub-li {
                        line-height: 15px !important
                    }
                    .level1 {
                        line-height: 15px !important
                    }
                    .ipad-category-title-sub {
                        left: -5% !important;
                        margin-bottom: 0
                    }
                    #homepage-ul-left {
                        padding-top: 0 !important;
                        margin-top: 0%
                    }
                }

                .iphone-submenu {
                    margin-left: 0 !important;
                    width: 100% !important;
                    text-align: center !important;
                    height: auto !important;
                    padding-top: 0 !important
                }

                @media only screen and (min-width: 300px) and (max-width: 599px) {
                    #custom-submenu-sale {
                        border-bottom: 0;
                        border-top: 0px solid #fff
                    }
                    #menu-item-17 {
                        border: 0;
                        border-top: 2px solid #fff
                    }
                    .homepage-title-submenu>a {}
                    #navigation {
                        top: -3px
                    }
                    #homepage-ul-left {
                        padding: 0 !important
                    }
                    .home-page-instagram-sub-title {
                        font-family: "futura-light", "got-light", sans-serif;
                        display: inline
                    }
                    .homepage-block-1-h1-span-second {
                        font-size: 11pt;
                        display: block;
                        padding-top: 5px
                    }
                }

                @media only screen and (max-width: 769px) {
                    .nav-primary>li:first-child {
                        border-bottom: 2px solid #fff !important
                    }
                }

                #homepage-ul-left {
                    padding-top: 40px
                }

                @media only screen and (min-width: 771px) and (max-width: 1024px) {
                    #homepage-ul-left {
                        padding-top: 30px !important;
                        padding-left: 0
                    }
                    .submenus-container>ul li.sub-column {
                        width: 143px !important;
                        padding-left: 0
                    }
                    .ipad-category-title {
                        width: 140% !important
                    }
                    .ipad-category-title-second {
                        width: 270% !important
                    }
                    .homepage-submenu-left-ul:first-child {
                        margin-bottom: 14%;
                        margin-top: -10%
                    }
                }
            </style>
            <script type="text/javascript">
                $j = jQuery.noConflict();
                var htmlChildren = "<ul id=\"homepage-ul-left\" class=\"level0 \" style=\"display: none; width: auto; max-width: 100%; padding-top: 20px;\" data-parent=\"\">\r\n<li class=\"level1 sub-column column-left\" style=\"margin-right: 0;\">\r\n<ul class=\"homepage-submenu-left-ul\">\r\n<li class=\"column-title title-sale homepage-title-submenu\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/nouveautes\">Nouveaut&eacute;s<\/a><\/li>\r\n<li class=\"column-title title-sale homepage-title-submenu\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\">Toutes les collections<\/a><\/li>\r\n<li class=\"column-title title-sale homepage-title-submenu\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/intemporels\">Best Sellers<\/a><\/li>\r\n<\/ul>\r\n<ul class=\"homepage-submenu-left-ul\">\r\n<li class=\"column-title title-sale homepage-title-submenu\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/imprimes-animaux\">Imprim&eacute;s animaux<\/a><\/li>\r\n<li class=\"column-title title-sale homepage-title-submenu\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/initiales-brodees\">Initiales brod&eacute;es<\/a><\/li>\r\n<li class=\"column-title title-sale homepage-title-submenu\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/mariage\">Mariage<\/a><\/li>\r\n<li class=\"column-title title-sale homepage-title-submenu\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/haute-couture\">Haute Couture<\/a><\/li>\r\n<\/ul>\r\n<\/li>\r\n<li class=\"level1 sub-column column-right home-page-submenu-item\">\r\n<ul>\r\n<ul>\r\n<li class=\"column-title\">\r\n<h1 class=\"homepage-category-title ipad-category-title\" style=\"width: 100%; left: -10%; background-color: transparent; cursor: pointer;\" onclick=\"top.location.href='https:\/\/www.mychatelles.com\/fr\/collection'\"><span>nos <span class=\"text-second\">slippers<\/span><\/span><\/h1>\r\n<\/li>\r\n<\/ul>\r\n<\/ul>\r\n<img style=\"cursor: pointer;\" onclick=\"top.location.href='https:\/\/www.mychatelles.com\/fr\/collection\/slippers'\" alt=\"\" src=\"https:\/\/www.mychatelles.com\/media\/wysiwyg\/menu\/leo-2pieds_1_2.png\" height=\"370\" width=\"370\" \/>\r\n<ul class=\"homepage-submenu-ul\">\r\n<li id=\"menu-item-category-16\" class=\"homepage-subtite-first-title level1 nav-1-4\" data-image=\"\/\/media\/catalog\/category\/IMG_1013-en.jpg\"><a class=\"level1 \" href=\"https:\/\/www.mychatelles.com\/fr\/collection\/slippers\">les slippers<\/a><\/li>\r\n<li id=\"menu-item-category-29\" class=\"level1 nav-1-5\" data-image=\"\/\/media\/catalog\/category\/IMG_8306-en.jpg\"><a class=\"level1 homepage-submenu-right-sub-li\" href=\"https:\/\/www.mychatelles.com\/fr\/collection\/black-shoes\">Chaussures noires<\/a><\/li>\r\n<li id=\"menu-item-category-22\" class=\"level1 nav-1-6\" data-image=\"\/\/media\/catalog\/category\/janvier20-en.jpg\"><a class=\"level1 homepage-submenu-right-sub-li\" href=\"https:\/\/www.mychatelles.com\/fr\/collection\/summer-shoes\">Chaussures &eacute;t&eacute; <\/a><\/li>\r\n<\/ul>\r\n<\/li>\r\n<li class=\"level1 sub-column column-right home-page-submenu-item\">\r\n<ul>\r\n<ul>\r\n<li class=\"column-title\">\r\n<h1 class=\"homepage-category-title ipad-category-title-sub\" style=\"width: 100%; left: -10%; background-color: transparent;\">&nbsp;<\/h1>\r\n<\/li>\r\n<\/ul>\r\n<\/ul>\r\n<img style=\"cursor: pointer;\" onclick=\"top.location.href='https:\/\/www.mychatelles.com\/fr\/collaboration-le-meurice-x-chatelles'\" alt=\"\" src=\"https:\/\/www.mychatelles.com\/media\/wysiwyg\/menu\/s_raphin-2pieds_2_2.png\" height=\"370\" width=\"370\" \/>\r\n<ul class=\"homepage-submenu-ul\">\r\n<li id=\"menu-item-category-16\" class=\"homepage-subtite-first-title level1 nav-1-4\" data-image=\"\/\/media\/catalog\/category\/IMG_1013-en.jpg\"><a class=\"level1\" href=\"https:\/\/www.mychatelles.com\/fr\/collaboration\">les collaborations<\/a><\/li>\r\n<li class=\"level1 nav-1-5\" data-image=\"\"><a class=\"level1 homepage-submenu-right-sub-li\" href=\"https:\/\/www.mychatelles.com\/fr\/collaboration-elisabeth-von-thurn-und-taxis-x-chatelles#block-taxis\">x Elisabeth Thurn und Taxis<\/a><\/li>\r\n<li id=\"menu-item-category-29\" class=\"level1 nav-1-5\" data-image=\"\"><a class=\"level1 homepage-submenu-right-sub-li\" href=\"https:\/\/www.mychatelles.com\/fr\/collaboration-le-meurice-x-chatelles#block-meurice\">x Le Meurice<\/a><\/li>\r\n<li id=\"menu-item-category-22\" class=\"level1 nav-1-6\"><a class=\"level1 homepage-submenu-right-sub-li\" href=\"https:\/\/www.mychatelles.com\/fr\/collab#block-chocolat\">x La Maison du Chocolat<\/a><\/li>\r\n<li class=\"level1 nav-1-6\" data-image=\"\"><a class=\"level1 homepage-submenu-right-sub-li\" href=\"https:\/\/www.mychatelles.com\/fr\/princesse-tam-tam-x-chatelles#block-tam-tam\">x Princesse tam.tam<\/a><\/li>\r\n<\/ul>\r\n<\/li>\r\n<li class=\"level1 sub-column column-right home-page-submenu-item\">\r\n<ul>\r\n<ul>\r\n<li class=\"column-title\">\r\n<h1 class=\"homepage-category-title ipad-category-title-sub\" style=\"width: 100%; left: -10%; background-color: transparent;\">&nbsp;<\/h1>\r\n<\/li>\r\n<\/ul>\r\n<\/ul>\r\n<img style=\"cursor: pointer;\" onclick=\"top.location.href='https:\/\/www.mychatelles.com\/fr\/collection\/mini'\" alt=\"\" src=\"https:\/\/www.mychatelles.com\/media\/wysiwyg\/menu\/mini-theophile-2pieds.png\" height=\"370\" width=\"370\" \/>\r\n<ul class=\"homepage-submenu-ul\">\r\n<li id=\"menu-item-category-16\" class=\"homepage-subtite-first-title level1 nav-1-4\" data-image=\"\/\/media\/catalog\/category\/IMG_1013-en.jpg\"><a class=\"level1 \" href=\"https:\/\/www.mychatelles.com\/fr\/collection\/mini\">les minis<\/a><\/li>\r\n<li id=\"menu-item-category-29\" class=\"level1 nav-1-5\" data-image=\"\/\/media\/catalog\/category\/IMG_8306-en.jpg\"><a class=\"level1 homepage-submenu-right-sub-li\" href=\"https:\/\/www.mychatelles.com\/en\/collaboration-le-meurice-x-chatelles\">&nbsp;<\/a><\/li>\r\n<li class=\"level1 nav-1-5\" data-image=\"\/\/media\/catalog\/category\/IMG_8306-en.jpg\"><a class=\"level1 homepage-submenu-right-sub-li\" href=\"https:\/\/www.mychatelles.com\/en\/collaboration-elisabeth-von-thurn-und-taxis-x-chatelles\">&nbsp;<\/a><\/li>\r\n<li id=\"menu-item-category-22\" class=\"level1 nav-1-6\"><a class=\"level1 homepage-submenu-right-sub-li\" href=\"https:\/\/www.mychatelles.com\/en\/collab\">&nbsp;<\/a><\/li>\r\n<li class=\"level1 nav-1-6\" data-image=\"\/\/media\/catalog\/category\/janvier20-en.jpg\"><a class=\"level1 homepage-submenu-right-sub-li\" href=\"https:\/\/www.mychatelles.com\/en\/princesse-tam-tam-x-chatelles\">&nbsp;<\/a><\/li>\r\n<\/ul>\r\n<\/li>\r\n<li class=\"level1 sub-column column-right home-page-submenu-item\">\r\n<ul>\r\n<ul>\r\n<li class=\"column-title\">\r\n<h1 class=\"homepage-category-title ipad-category-title-second\" style=\"width: 200%; left: -10%; background-color: transparent; cursor: pointer;\" onclick=\"top.location.href='https:\/\/www.mychatelles.com\/en\/collection'\"><span>nos variations <span class=\"text-second\">de slippers<\/span><\/span><\/h1>\r\n<\/li>\r\n<\/ul>\r\n<\/ul>\r\n<img style=\"cursor: pointer;\" onclick=\"top.location.href='https:\/\/www.mychatelles.com\/fr\/collection\/mules'\" alt=\"\" src=\"https:\/\/www.mychatelles.com\/media\/wysiwyg\/menu\/romuald-2pieds_1_5_2.png\" height=\"370\" width=\"370\" \/>\r\n<ul class=\"homepage-submenu-ul\">\r\n<li id=\"menu-item-category-16\" class=\"homepage-subtite-first-title level1 nav-1-4\" data-image=\"\/\/media\/catalog\/category\/IMG_1013-en.jpg\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/mules\">les Mules<\/a><\/li>\r\n<\/ul>\r\n<\/li>\r\n<li class=\"level1 sub-column column-right home-page-submenu-item\">\r\n<ul>\r\n<ul>\r\n<li class=\"column-title\">\r\n<h1 class=\"homepage-category-title ipad-category-title-sub\" style=\"width: 100%; left: -10%; background-color: transparent;\">&nbsp;<\/h1>\r\n<\/li>\r\n<\/ul>\r\n<\/ul>\r\n<img style=\"cursor: pointer;\" onclick=\"top.location.href='https:\/\/www.mychatelles.com\/fr\/collection\/pointy'\" alt=\"\" src=\"https:\/\/www.mychatelles.com\/media\/wysiwyg\/menu\/anatole-2pieds_2.png\" height=\"370\" width=\"370\" \/>\r\n<ul class=\"homepage-submenu-ul\">\r\n<li id=\"menu-item-category-16\" class=\"homepage-subtite-first-title level1 nav-1-4\" data-image=\"\/\/media\/catalog\/category\/IMG_1013-en.jpg\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/pointy\">les pointus<\/a><\/li>\r\n<\/ul>\r\n<\/li>\r\n<li class=\"level1 sub-column column-right home-page-submenu-item\">\r\n<ul>\r\n<ul>\r\n<li class=\"column-title\">\r\n<h1 class=\"homepage-category-title ipad-category-title-sub\" style=\"width: 100%; left: -10%; background-color: transparent;\">&nbsp;<\/h1>\r\n<\/li>\r\n<\/ul>\r\n<\/ul>\r\n<img style=\"cursor: pointer;\" onclick=\"top.location.href='https:\/\/www.mychatelles.com\/fr\/collection\/slip-ons-154'\" alt=\"\" src=\"https:\/\/www.mychatelles.com\/media\/wysiwyg\/menu\/arnold-slip-on-2pieds_2_2.png\" height=\"370\" width=\"370\" \/>\r\n<ul class=\"homepage-submenu-ul\">\r\n<li id=\"menu-item-category-16\" class=\"homepage-subtite-first-title level1 nav-1-4\" data-image=\"\/\/media\/catalog\/category\/IMG_1013-en.jpg\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/slip-ons-154\">les slip-ons<\/a><\/li>\r\n<\/ul>\r\n<\/li>\r\n<li class=\"clear\"><\/li>\r\n<\/ul>";
                var htmlChildrenIphone = "<ul id=\"homepage-ul-left\" class=\"level0 \" style=\"height: 349px; display: none; width: auto; max-width: 100%;\" data-parent=\"\">\r\n<li class=\"level1 sub-column column-left iphone-submenu\" style=\"margin-right: 0;\">\r\n<ul>\r\n<li class=\"column-title title-sale homepage-title-submenu\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/nouveautes\">Nouveaut&eacute;s<\/a><\/li>\r\n<li class=\"column-title title-sale homepage-title-submenu\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\">Toutes les collections<\/a><\/li>\r\n<li class=\"column-title title-sale homepage-title-submenu\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/intemporels\">Best Sellers<\/a><\/li>\r\n<\/ul>\r\n<p>&nbsp;<\/p>\r\n<ul>\r\n<li class=\"column-title title-sale homepage-title-submenu\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/imprimes-animaux\">Imprim&eacute;s animaux<\/a><\/li>\r\n<li class=\"column-title title-sale homepage-title-submenu\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/initiales-brodees\">Initiales brod&eacute;es<\/a><\/li>\r\n<li class=\"column-title title-sale homepage-title-submenu\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/mariage\">Mariage<\/a><\/li>\r\n<li class=\"column-title title-sale homepage-title-submenu\"><a href=\"https:\/\/www.mychatelles.com\/fr\/collection\/haute-couture\">Haute Couture<\/a><\/li>\r\n<\/ul>\r\n<\/li>\r\n<li class=\"clear\"><\/li>\r\n<\/ul>";
                var titleOfMenu = "Soldes 2016";
                var linkOfMenu = "https://www.mychatelles.com/fr/collection/soldes-1";
                var _id = "";
                $j(document).ready(function() {
                    if ($j(window).width() > 1024) {
                        _id = $j(".nav-primary").find("li").first().attr("id");
                        setTimeout(function() {
                            $j(".submenus-container").find("ul")[0].remove();
                        }, 500);
                        setTimeout(function() {
                            $j(".submenus-container").prepend(htmlChildren);
                            $j("#homepage-ul-left").show(0);
                            $j("#homepage-ul-left").attr("data-parent", _id);
                        }, 500);
                    }
                    if ($j(window).width() > 786 && $j(window).width() <= 1024) {
                        _id = $j(".nav-primary").find("li").first().attr("id");
                        setTimeout(function() {
                            $j(".submenus-container").find("ul[data-parent='" + _id + "']")[0].remove()
                            $j("#navigation").find("ul[data-parent='" + _id + "']").remove();
                        }, 500);
                        setTimeout(function() {
                            $j(".submenus-container").prepend(htmlChildren);
                            $j("#homepage-ul-left").show(0);
                            $j("#homepage-ul-left").attr("data-parent", _id);
                        }, 500);
                    }
                    if ($j(window).width() > 600 && $j(window).width() < 785) {
                        _id = $j(".nav-primary").find("li").first().attr("id");
                        setTimeout(function() {
                            $j(".nav-primary").find("li").first().find("ul")[0].remove();
                        }, 500);
                        setTimeout(function() {
                            $j(".nav-primary").find("li").first().append(htmlChildren);
                            $j("#homepage-ul-left").attr("data-parent", _id);
                        }, 500);
                    }
                    if ($j(window).width() > 300 && $j(window).width() < 599) {
                        _id = $j(".nav-primary").find("li").first().attr("id");
                        setTimeout(function() {
                            $j(".nav-primary").find("li").first().find("ul")[0].remove();
                        }, 500);
                        setTimeout(function() {
                            $j(".nav-primary").find("li").first().append(htmlChildrenIphone);
                            $j("#homepage-ul-left").attr("data-parent", _id);
                        }, 500);
                    }
                });
                $j(window).resize(function() {
                    if ($j(window).width() > 786 && $j(window).width() <= 1024) {
                        _id = $j(".nav-primary").find("li").first().attr("id");
                        console.log("resize" + _id);
                        setTimeout(function() {
                            $j(".submenus-container").find("ul[data-parent='" + _id + "']")[0].remove()
                            $j("#navigation").find("ul[data-parent='" + _id + "']").remove();
                        }, 500);
                        setTimeout(function() {
                            $j(".submenus-container").prepend(htmlChildren);
                            $j("#homepage-ul-left").show(0);
                            $j("#homepage-ul-left").attr("data-parent", _id);
                        }, 500);
                    }
                    if ($j(window).width() > 600 && $j(window).width() < 785) {
                        _id = $j(".nav-primary").find("li").first().attr("id");
                        setTimeout(function() {
                            $j(".nav-primary").find("li").first().find("ul")[0].remove();
                        }, 500);
                        setTimeout(function() {
                            $j(".nav-primary").find("li").first().append(htmlChildren);
                            $j("#homepage-ul-left").attr("data-parent", _id);
                        }, 500);
                    }
                });

                function openCustomSubmenuOnIpad() {
                    if ($j("#custom-submenu-sale").hasClass("custom-active")) {
                        closeSubMenu();
                        $j("#custom-submenu-sale").removeClass("custom-active");
                    } else {
                        $j("#custom-submenu-sale").toggleClass("custom-active");
                        $j("#homepage-sale").attr("style", "height:400px;display:block !important");
                        $j("#homepage-sale").fadeIn(300);
                        $j("#homepage-ul-left").fadeIn(300);
                        return;
                    }
                }

                function openCustomSubmenuOnIphone() {
                    if ($j("#custom-submenu-sale").hasClass("custom-active")) {
                        closeSubMenu();
                        $j("#custom-submenu-sale").removeClass("custom-active");
                    } else {
                        $j("#custom-submenu-sale").toggleClass("custom-active");
                        $j("#homepage-sale").attr("style", "height:auto;display:block !important");
                        $j("#homepage-sale").fadeIn(300);
                        $j("#homepage-ul-left").fadeIn(300);
                        return;
                    }
                }

                function openRedirectLinkMenu() {
                    top.location = linkOfMenu;
                }

                function openParentMenu(elem) {
                    if ($j(elem).hasClass("skip-active")) {
                        $j(elem).removeClass("skip-active");
                        var $_id = "#header-nav";
                        $j($_id).removeClass("skip-active");
                    } else {
                        $j(elem).addClass("skip-active");
                        var $_id = "#header-nav";
                        $j($_id).addClass("skip-active");
                    }
                }

                function openSubMenuIpad(elem) {
                    console.log(_id);
                    console.log(elem.id);
                    if (elem.id == _id) {
                        console.log($j("#homepage-ul-left").css("display"));
                        if ($j("#homepage-ul-left").css("display") == "none") {
                            $j(elem).toggleClass("menu-active");
                            document.getElementById("homepage-ul-left").style.display = "block";
                            $j(".submenus-container").css("height", "auto !important");
                            console.log("opened");
                        } else {
                            $j(elem).removeClass("menu-active");
                            document.getElementById("homepage-ul-left").style.display = "none";
                        }
                    }
                }

                function openSubMenuIpadApp(elemApp) {
                    var elem = $j(elemApp);
                    if (elem.attr("id") == _id) {
                        if ($j("#homepage-ul-left").css("display") == "none") {
                            document.getElementById("homepage-ul-left").style.display = "block";
                        } else {
                            document.getElementById("homepage-ul-left").style.display = "none";
                        }
                    }
                    if ($j(window).width() > 786 && $j(window).width() <= 1024) {
                        if (elem.attr("id") == _id) {
                            console.log(document.getElementById("homepage-ul-left").style.display);
                            if ($j("#homepage-ul-left").hasClass("customActive")) {
                                $j(".submenus-container").css("height", "0px");
                                $j("#homepage-ul-left").hide();
                                $j("#homepage-ul-left").removeClass("customActive");
                            } else {
                                $j(".submenus-container").css("height", "auto");
                                $j("#homepage-ul-left").show();
                                $j("#homepage-ul-left").addClass("customActive");
                            }
                        }
                    }
                }
            </script>
            <link rel="stylesheet" type="text/css" href="https://www.mychatelles.com/skin/frontend/base/default/responsiveslider/css/generic-nav.css" media="all">
            <link rel="stylesheet" type="text/css" href="https://www.mychatelles.com/skin/frontend/base/default/responsiveslider/css/grid12.css" media="all">
            <script type="text/javascript" src="https://www.mychatelles.com/skin/frontend/base/default/responsiveslider/js/jquery_003.js"></script>
            <script type="text/javascript" src="https://www.mychatelles.com/skin/frontend/base/default/responsiveslider/js/jquery.easing.min.js"></script>
            <div class="the-slideshow gen-slider-arrows2 gen-slider-pager1 gen-slider-pager1-pos-bottom-right grid12-9  ">
                <ul class="slides" style="padding:0px!important;">
									<?php
									include_once("model/slider.php");
									$slider = new Slider();
									$objectSliders=$slider->GetListSlider();
									foreach($objectSliders as $_slider){
										$image = $_slider->getSliderImage();
										$title = $_slider->getSliderTitle();
										$description = $_slider->getSliderDescription();
										$pathImageSlider = $pathImage."".$image;
									?>
                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;" class="slide">
                        <a href="#">
                            <img src="<?php echo $pathImageSlider; ?>" alt="<?php echo $title ?>" title="<?php echo $title; ?>" style="/*width:100%;height:auto;*/width:100%;height:671px" />
                            <div class="caption dark3 home-page-caption-slider">
                                <h2>
																	<span class='description-first' style='font-size:80px;line-height:120px;font-weight:800;color:#ffffff;display:inline'><?php echo $title; ?></span>
																	<br/>
																	<span class='description-second' style='font-size:60px;line-height:120px;font-weight:300;color:#ffffff;display:inline'><?php echo $description ?></span></h2>
                                <h2 style="margin-top: -50px" class="description-third"><span style='display:inline'></span></h2>
                            </div>
                        </a>
                    </li>
										<?php } ?>
                </ul>
            </div>
            <script type="text/javascript">
                jQuery(function($) {
                    $('.the-slideshow').flexslider({
                        namespace: "",
                        animation: 'fade',
                        easing: 'easeInQuad',
                        useCSS: true,
                        animationLoop: true,
                        smoothHeight: true,
                        slideshowSpeed: 5000,
                        animationSpeed: 1000,
                        pauseOnHover: false
                    });
                });
            </script>
            <div class="clearfix"></div>
            <div class="main-container col1-layout" style="background-color: rgb(246,246,246);padding-top:0 !important;margin-top:0%;">
                <div class="main">
                    <div class="col-main">
                        <div class="ccrow">
                            <article class="homepage-article">
                                <div class="homepage-block-1" onclick="top.location.href='#'">
                                    <h1 class="homepage-block-1-h1 homepage-responsive-h1" style="margin-top: 10px;">
                                        <span class="homepage-block-1-h1-span-first">Phương Châm</span>
                                        <span class="homepage-block-1-h1-span-second">Dẫn Đầu Xu Thế. Khẳng Định Chất Tôi</span></h1>
                                    <p class="homepage-block-1-content">Dẫn đầu xu thế, khẳng định chất tôi với các dòng điện thoại thông minh được chế tác tinh xảo cùng tính năng kháng nước, chống bụi độc đáo.</p>
                                    <p class="homepage-block-1-collection" align="center">
                                        <a href="#">Sản Phẩm</a></p>
                                </div>
                            </article>
                            <div class="cc col-1-3" id="renderfirstblock">
                                <!--<img src="https://www.mychatelles.com/skin/frontend/myc/myctheme/images/ajax-loader.gif" style="width:auto;height:auto;margin: auto" />-->
                                <div class="block-1">
    <ul class="" id="owl-also-like">
      <?php
      include_once("model/product.php");
      $product = new Product();
      //Iphone 7
      $objectArrayProduct=$product->getProductByCategory(1);
      foreach($objectArrayProduct as $_product){
        $imgProduct=$pathImage."".$_product->getProductImage();
        $nameProduct=$_product->getProductName();
       ?>
      <li class="">
            <a href="#" title="<?php echo $nameProduct; ?>">
                  <img class="home-page-products main-img" width="370" height="370" id="product-collection-image-1465" src="<?php echo $imgProduct; ?>" style="display: block;">
            </a>
            <div class="home-page-product-infor">
                <h2 class="home-page-product-name">
                    <a href="#" title="<?php echo $nameProduct; ?>"><?php echo $nameProduct; ?></a>
                </h2>
            </div>
        </li>
        <?php } ?>
    </ul>
    <div class="bx-controls bx-has-controls-direction">
        <div class="bx-controls-direction">
            <a class="home-page-prev prev-has-description" href="#"></a>
            <a class="home-page-next prev-has-description" href="#"></a>
        </div>
    </div>
</div>
<script>
    $j = jQuery.noConflict();
    $j(document).ready(function() {
        var owl_also_like = $j("#owl-also-like");
        owl_also_like.owlCarousel({
            pagination: false,
            items: 2, //10 items above 1000px browser width
            itemsDesktop: [1000, 4], //5 items between 1000px and 901px
            itemsDesktopSmall: [900, 2], // betweem 900px and 601px
            itemsTablet: [600, 2], //2 items between 600 and 0
            itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
            rewindSpeed: 1
        });
        // Custom Navigation Events
        $j(".home-page-next").click(function() {
            var owl_next = $j("#owl-also-like").data('owlCarousel');
            owl_next.next();
            return false;
        });
        $j(".home-page-prev").click(function() {
            var owl_next = $j("#owl-also-like").data('owlCarousel');
            owl_next.prev();
            return false
        });

    });
</script>
                              </div>
                            <div class="cc col-2-3">
                              <?php
                              include_once("model/category.php");
                              $category = new Category();
                              $objectCategoryLeft = $category->getCategory(1);
                              $pathImageLeft = $pathImage."".$objectCategoryLeft->getCategoryImage();
                              ?>
                                <div class="block-1" style="position: relative">
                                    <h1 class="homepage-category-title"><span><?php echo $objectCategoryLeft->getCategoryName(); ?></span></h1>
                                    <a href="#" class="plus" style="top: 15%;left: 55%;margin-top: -50px;z-index: 0;border-radius: 50%;">
                                        <span alt="" class="plus-img" style=";width:25px !important;height:25px !important"></span>
                                    </a>
                                    <a href="#">
                                      <img alt="" src="<?php echo $pathImageLeft;  ?>"></a>
                                </div>
                            </div>
                            <div class="cc col-1-3" style="margin-top: -5%">
                              <?php
                              $objectCategoryRight = $category->getCategory(2);
                              $pathImageRight = $pathImage."".$objectCategoryRight->getCategoryImage();
                              ?>
                                <div class="block-1">
                                    <a href="#" class="plus" style="top: 40%;left: 35%;margin-top: -50px;z-index: 0;border-radius: 50%;">
                                        <span alt="" class="plus-img" style=";width:25px !important;height:25px !important"></span>
                                    </a>
                                    <h1 class="homepage-category-title"><span><?php echo $objectCategoryRight->getCategoryName(); ?></span></h1>
                                    <a href="#"><img alt="" src="<?php echo $pathImageRight; ?>"></a>
                                </div>
                            </div>
                            <div class="cc col-2-3" style="margin-top: 3%;" id="rendersecondblock">
                                <div class="block-1">
                                    <ul class="" id="owl-also-like-2">
                                        <?php
                                        $product = new Product();
                                        //Sam Sung
                                        $objectArrayProduct=$product->getProductByCategory(2);
                                        foreach($objectArrayProduct as $_product){
                                            $imgProduct=$pathImage."".$_product->getProductImage();
                                            $nameProduct=$_product->getProductName();
                                            ?>
                                            <li class="">
                                                <a href="#" title="<?php echo $nameProduct; ?>">
                                                    <img class="home-page-products main-img" width="370" height="370" id="product-collection-image-1465" src="<?php echo $imgProduct; ?>" style="display: block;">
                                                </a>
                                                <div class="home-page-product-infor">
                                                    <h2 class="home-page-product-name">
                                                        <a href="#" title="<?php echo $nameProduct; ?>"><?php echo $nameProduct; ?></a>
                                                    </h2>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                    <div class="bx-controls bx-has-controls-direction">
                                        <div class="bx-controls-direction">
                                            <a class="home-page-prev prev-has-description" href="#"></a>
                                            <a class="home-page-next prev-has-description" href="#"></a>
                                        </div>
                                    </div>
                                </div>
                                <script>
                                    $j = jQuery.noConflict();
                                    $j(document).ready(function() {
                                        var owl_also_like = $j("#owl-also-like-2");
                                        owl_also_like.owlCarousel({
                                            pagination: false,
                                            items: 2, //10 items above 1000px browser width
                                            itemsDesktop: [1000, 4], //5 items between 1000px and 901px
                                            itemsDesktopSmall: [900, 2], // betweem 900px and 601px
                                            itemsTablet: [600, 2], //2 items between 600 and 0
                                            itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
                                            rewindSpeed: 1
                                        });
                                        // Custom Navigation Events
                                        $j(".home-page-next").click(function() {
                                            var owl_next = $j("#owl-also-like-2").data('owlCarousel');
                                            owl_next.next();
                                            return false;
                                        });
                                        $j(".home-page-prev").click(function() {
                                            var owl_next = $j("#owl-also-like-2").data('owlCarousel');
                                            owl_next.prev();
                                            return false
                                        });

                                    });
                                </script>
                            </div>
                        <div class="clearfix"></div>
                        <div class="ccrow homepage-block-3-cate">
                            <div class="cc homepage-3-blocks">
                                <figure>
                                    <p class="homepage-3-categories-img">
                                        <a href="#" class="plus" style="top:45%;left:30%;margin-top: -50px;z-index: 0;border-radius: 50%;">
                                            <span alt="" class="plus-img" style=";width:25px !important;height:25px !important"></span>
                                        </a>
                                        <a href="#">
                                            <img alt="" src="images/tra_gop.jpg">
                                        </a>
                                    </p>
                                    <h2 class="homepage-3-categories-href">
                                        <a href="#">
                                            <span class="homepage-3-categories-title">Ưu đãi trả góp, lãi suất 0%</span>
                                        </a>
                                    </h2>
                                    <p class="homepage-3-categories-description">Với mục tiêu mang đến cho Quý khách hàng những giá trị dịch vụ tối đa và thay cho lời tri ân, Cửa hàng cùng Ngân hàng sẽ triển khai chương trình mua sắm ưu đãi dành cho các chủ thẻ tín dụng Vietinbank, đó là: “Mua trả góp lãi suất 0% trong 12 tháng” cho tất cả sản phẩm.</p>
                                </figure>
                            </div>
                            <div class="cc homepage-3-blocks">
                                <figure>
                                    <p class="homepage-3-categories-img">
                                        <a href="#" class="plus" style="top: 40%;left: 70%;margin-top: -50px;z-index: 0;border-radius: 50%;">
                                            <span alt="" class="plus-img" style=";width:25px !important;height:25px !important"></span>
                                        </a>
                                        <a href="#"><img alt="" src="images/xe-dap-xanh-chinh-sach-doi-tra-hang.jpg"></a>
                                    </p>
                                    <h2 class="homepage-3-categories-href">
                                        <a href="#">
                                            <span class="homepage-3-categories-title">Chính Sách Đổi Trả Hàng</span>
                                        </a>
                                    </h2>
                                    <p class="homepage-3-categories-description">Đổi trả hàng nếu sản phẩm không hài lòng quý khách. Hoàn tiền 100%</p>
                                </figure>
                            </div>
                            <div class="cc homepage-3-blocks">
                                <figure>
                                    <p class="homepage-3-categories-img">
                                        <a href="#"><img alt="" src="images/giao_hang.jpg"></a>
                                    </p>
                                    <h2 class="homepage-3-categories-href">
                                        <a href="#">
                                            <span class="homepage-3-categories-title">Giao Hàng Tận Nơi</span>
                                        </a>
                                    </h2>
                                    <p class="homepage-3-categories-description">Giao hàng toàn quốc , nhanh chóng , miến phí ship </p>
                                </figure>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ccrow home-page-you-also" style="margin-top: 5%">
                            <div style="width: 100%;margin: auto" class="home-page-you-also-block-1">
                                <div id="renderthirdblock">
                                    <div class="block-1">
                                        <ul class="" id="owl-missing-also-like-two">
                                            <?php
                                            $product = new Product();
                                            //Sam Sung
                                            $objectArrayProduct=$product->getProducts();
                                            foreach($objectArrayProduct as $_product){
                                                $imgProduct=$pathImage."".$_product->getProductImage();
                                                $nameProduct=$_product->getProductName();
                                                ?>
                                                <li class="">
                                                    <a href="#" title="<?php echo $nameProduct; ?>">
                                                        <img class="home-page-products main-img" width="328" height="394" id="product-collection-image-1465" src="<?php echo $imgProduct; ?>" style="display: block;width: 328px !important;height:394px !important;">
                                                    </a>
                                                    <div class="home-page-product-infor">
                                                        <h2 class="home-page-product-name">
                                                            <a href="#" title="<?php echo $nameProduct; ?>"><?php echo $nameProduct; ?></a>
                                                        </h2>
                                                    </div>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                        <div class="bx-controls bx-has-controls-direction">
                                            <div class="bx-controls-direction">
                                                <a class="home-page-prev prev-has-description" href="#"></a>
                                                <a class="home-page-next prev-has-description" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        $j = jQuery.noConflict();
                                        $j(document).ready(function() {
                                            var owl_also_like = $j("#owl-missing-also-like-two");
                                            owl_also_like.owlCarousel({
                                                pagination: false,
                                                items: 3, //10 items above 1000px browser width
                                                itemsDesktop: [1000, 4], //5 items between 1000px and 901px
                                                itemsDesktopSmall: [900, 2], // betweem 900px and 601px
                                                itemsTablet: [600, 2], //2 items between 600 and 0
                                                itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
                                                rewindSpeed: 1
                                            });
                                            // Custom Navigation Events
                                            $j(".home-page-next").click(function() {
                                                var owl_next = $j("#owl-missing-also-like-two").data('owlCarousel');
                                                owl_next.next();
                                                return false;
                                            });
                                            $j(".home-page-prev").click(function() {
                                                var owl_next = $j("#owl-missing-also-like-two").data('owlCarousel');
                                                owl_next.prev();
                                                return false
                                            });

                                        });
                                    </script>
                                </div>
                                <article class="homepage-article" style="background-color: rgb(246,246,246);margin-bottom: 0;padding-top: 30px;">
                                    <div class="homepage-block-1" style="padding-top: 0; cursor: pointer">
                                        <h1 class="homepage-block-1-h1 homepage-responsive-h1">
                                            <span class="homepage-block-1-h1-span-first">Đừng bỏ lỡ sản phẩm mới</span>
                                        </h1>
                                        <p class="homepage-block-1-content" style="text-align: center">
                                            Cửa hàng chúng tôi luôn cung cấp máy chính hảng, like new và chất lượng tốt. Hãy xem các sản phẩm của chúng tôi</p>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="follow">
                            <div class="home-page-instagram">
                                <div>
                                    <h1 class="homepage-block-1-h1 instagram-h1">
                                        <span class="homepage-block-1-h1-span-second" style="font-family: 'futura-light', 'got-light', sans-serif">Theo Dõi</span>
                                        <p>
                                            <a href="#">
                                                <img alt="" style="margin-bottom: 5px;" src="https://www.mychatelles.com/skin/frontend/myc/myctheme//images/InstagramLogo.png"></a>
                                            <span class="home-page-instagram-sub-title" style="text-transform: uppercase;margin-top: -5px;display:inline;font-size: 11pt;font-family: 'futura-light','got-light',sans-serif;"><strong>@LOGO #logo</strong></span></p>
                                        <br/>
                                    </h1>
                                </div>
                            </div>
                        </div>
                        <div class="follow" style="margin-top: -30px;padding: 0 !important">
                            <div class="jcarousel-wrapper">
                                <div class="jcarousel">
                                    <ul id="instafeed">
                                        <!--<img src="https://www.mychatelles.com/skin/frontend/myc/myctheme/images/ajax-loader.gif" style="width:auto;height:auto;margin: auto" />-->
                                    </ul>
                                    <div class="clear"></div>
                                    <div id="load-more" data-page="1" data-limit="5" data-url="#">
                                        Xem thêm</div>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</body>
</html>
